# -*- coding: utf-8 -*-
import subprocess
import ConfigParser
import sys
import os
import shutil

def get_setup_params():
    config_user = ConfigParser.ConfigParser()
    config_user.read(sys.argv[1])
    duration = int(config_user.get("main", "duration"))
    iterations = int(config_user.get("main", "iterations"))
    experiment = config_user.get("main", "experiment")
    out_path = config_user.get("main", "out_path")
    _freqs = config_user.get("main", "freqs")
    _freqs = _freqs.split(",")
    freqs = []
    for n in range(int(_freqs[2])):
        freqs += [float(_freqs[0]) + float(_freqs[1])*n]

    return (duration, iterations, freqs, experiment, out_path)

def set_experiment_params(freq):
    config_user = ConfigParser.ConfigParser()
    config_user.read(sys.argv[1])


    prev_event = "000"
    config_experiment_file = os.path.join(config_user.get(
        "main", "out_path"), "config_experiment.ini")
    if os.path.isfile(config_experiment_file):
        config_experiment = ConfigParser.ConfigParser()
        config_experiment.read(config_experiment_file)
        # Read previous event
        prev_event = config_experiment.get("main", "event")

    shutil.copyfile(sys.argv[1], config_experiment_file)
    config_experiment = ConfigParser.ConfigParser()
    config_experiment.read(config_experiment_file)

    # Increment event
    event = str(int(prev_event) + 1).zfill(3)
    config_experiment.set("main", "event", event)

    # set freq
    config_experiment.set("main", "freq", str(freq))

    with open(config_experiment_file, 'w') as cf:
        config_experiment.write(cf)

    return event




def main():
    # Get setup params
    duration, iterations, freqs, experiment, out_path = get_setup_params()
    print "[i] Iterations: %s, Dur: %s, Freqs: %s, Exp: %s" % (iterations, duration, freqs, experiment)



    for f in freqs:
        for i in range(iterations):

            # Set params into the experiment config file
            event = set_experiment_params(f)
            # print "[i] Event:", event
            print "[i] Iter: %s, Event: %s, Freq: %s" % (i, event, f)

            # Run experiment
            subprocess.call(['python', 'burst_gr_proc.py', str(duration)])

    shutil.copyfile(sys.argv[1], os.path.join(out_path, "exp" + experiment + ".ini"))


if __name__ == "__main__":
    main()