# -*- coding: utf-8 -*-

from burst import burst
import ConfigParser
import sys
from PyQt4 import Qt
from gnuradio import gr
import os
from shutil import copyfile
from time import sleep
import gc



def main():
    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    def stop_experiment():
        # print "Stopping experiment!"

        quitting()
        qapp.quit
        qapp.closeAllWindows()


    tb = burst()

    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()

    from PyQt4.QtCore import QTimer
    timer = QTimer()
    timer.timeout.connect(stop_experiment)

    timer.start(int(sys.argv[1]))

    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    # qapp.exec_()
    sys.exit(qapp.exec_())


if __name__ == "__main__":
    main()
