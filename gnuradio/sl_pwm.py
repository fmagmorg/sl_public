# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: SipLab PWM
# Generated: Thu May 24 15:05:11 2018
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes


class sl_pwm(gr.hier_block2):

    def __init__(self, amp=1, delay_percent=0.5, duty_cycle=0.5, freq=250, samp=32000):
        gr.hier_block2.__init__(
            self, "SipLab PWM",
            gr.io_signature(0, 0, 0),
            gr.io_signature(1, 1, gr.sizeof_float*1),
        )

        ##################################################
        # Parameters
        ##################################################
        self.amp = amp
        self.delay_percent = delay_percent
        self.duty_cycle = duty_cycle
        self.freq = freq
        self.samp = samp

        ##################################################
        # Variables
        ##################################################
        self.delay_samples = delay_samples = delay_percent*samp/freq

        ##################################################
        # Blocks
        ##################################################
        self.threshold_0 = blocks.threshold_ff(1-duty_cycle, 1-duty_cycle, 0)
        self.mult_0 = blocks.multiply_const_vff((-1, ))
        self.blocks_delay_0 = blocks.delay(gr.sizeof_float*1, int(delay_samples))
        self.analog_sig_source_x_0_0 = analog.sig_source_f(samp, analog.GR_SAW_WAVE, freq, 1, 0)
        self.amplitudeGain_0 = blocks.multiply_const_vff((amp, ))
        self.add_0 = blocks.add_const_vff((1, ))



        ##################################################
        # Connections
        ##################################################
        self.connect((self.add_0, 0), (self.threshold_0, 0))
        self.connect((self.amplitudeGain_0, 0), (self, 0))
        self.connect((self.analog_sig_source_x_0_0, 0), (self.blocks_delay_0, 0))
        self.connect((self.blocks_delay_0, 0), (self.mult_0, 0))
        self.connect((self.mult_0, 0), (self.add_0, 0))
        self.connect((self.threshold_0, 0), (self.amplitudeGain_0, 0))

    def get_amp(self):
        return self.amp

    def set_amp(self, amp):
        self.amp = amp
        self.amplitudeGain_0.set_k((self.amp, ))

    def get_delay_percent(self):
        return self.delay_percent

    def set_delay_percent(self, delay_percent):
        self.delay_percent = delay_percent
        self.set_delay_samples(self.delay_percent*self.samp/self.freq)

    def get_duty_cycle(self):
        return self.duty_cycle

    def set_duty_cycle(self, duty_cycle):
        self.duty_cycle = duty_cycle
        self.threshold_0.set_hi(1-self.duty_cycle)
        self.threshold_0.set_lo(1-self.duty_cycle)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.set_delay_samples(self.delay_percent*self.samp/self.freq)
        self.analog_sig_source_x_0_0.set_frequency(self.freq)

    def get_samp(self):
        return self.samp

    def set_samp(self, samp):
        self.samp = samp
        self.set_delay_samples(self.delay_percent*self.samp/self.freq)
        self.analog_sig_source_x_0_0.set_sampling_freq(self.samp)

    def get_delay_samples(self):
        return self.delay_samples

    def set_delay_samples(self, delay_samples):
        self.delay_samples = delay_samples
        self.blocks_delay_0.set_dly(int(self.delay_samples))
