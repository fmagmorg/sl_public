#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Burst
# Generated: Tue Dec 11 14:18:29 2018
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from PyQt4 import Qt
from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
from sl_pwm import sl_pwm  # grc-generated hier_block
import ConfigParser
import red_pitaya
import sip
from gnuradio import qtgui


class burst(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Burst")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Burst")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "burst")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.results_path = results_path = "/tmp/rx_signals/"
        self.config_file_name = config_file_name = "config_experiment.ini"
        self._freq_sqr_config = ConfigParser.ConfigParser()
        self._freq_sqr_config.read(results_path + config_file_name)
        try: freq_sqr = self._freq_sqr_config.getfloat('main', "freq_sqr")
        except: freq_sqr = 0
        self.freq_sqr = freq_sqr
        self._freq_config = ConfigParser.ConfigParser()
        self._freq_config.read(results_path + config_file_name)
        try: freq = self._freq_config.getfloat('main', "freq")
        except: freq = 1
        self.freq = freq
        self._samp_rate_config = ConfigParser.ConfigParser()
        self._samp_rate_config.read(results_path + config_file_name)
        try: samp_rate = self._samp_rate_config.getfloat('main', "samp_rate")
        except: samp_rate = 0
        self.samp_rate = samp_rate
        self._experiment_config = ConfigParser.ConfigParser()
        self._experiment_config.read(results_path + config_file_name)
        try: experiment = self._experiment_config.get('main', "experiment")
        except: experiment = "0"
        self.experiment = experiment
        self._event_config = ConfigParser.ConfigParser()
        self._event_config.read(results_path + config_file_name)
        try: event = self._event_config.get('main', "event")
        except: event = "000"
        self.event = event
        self.dcycle = dcycle = 5.*freq_sqr/freq
        self._center_freq_config = ConfigParser.ConfigParser()
        self._center_freq_config.read(results_path + config_file_name)
        try: center_freq = self._center_freq_config.getfloat('main', "center_freq")
        except: center_freq = 0
        self.center_freq = center_freq
        self.result_prefix = result_prefix = str(results_path) + "exp"+str(experiment) +  "_e"+str(event) +"_sr"+str(samp_rate)+"_dc"+str(dcycle)+"_fc"+str(center_freq)+"_fsqr"+str(freq_sqr) +  "_f" + str(freq) + "_"
        self.variable_qtgui_label_8 = variable_qtgui_label_8 = center_freq
        self.variable_qtgui_label_7 = variable_qtgui_label_7 = experiment
        self.variable_qtgui_label_6 = variable_qtgui_label_6 = event
        self.variable_qtgui_label_5 = variable_qtgui_label_5 = freq_sqr
        self.variable_qtgui_label_4 = variable_qtgui_label_4 = freq
        self.variable_qtgui_label_3 = variable_qtgui_label_3 = dcycle
        self.variable_qtgui_label_21 = variable_qtgui_label_21 = result_prefix
        self.variable_qtgui_label_2 = variable_qtgui_label_2 = config_file_name
        self.variable_qtgui_label_1 = variable_qtgui_label_1 = results_path
        self.variable_qtgui_label_0 = variable_qtgui_label_0 = samp_rate

        ##################################################
        # Blocks
        ##################################################
        self._variable_qtgui_label_8_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_8_formatter = None
        else:
          self._variable_qtgui_label_8_formatter = lambda x: repr(x)

        self._variable_qtgui_label_8_tool_bar.addWidget(Qt.QLabel('center_freq'+": "))
        self._variable_qtgui_label_8_label = Qt.QLabel(str(self._variable_qtgui_label_8_formatter(self.variable_qtgui_label_8)))
        self._variable_qtgui_label_8_tool_bar.addWidget(self._variable_qtgui_label_8_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_8_tool_bar)
        self._variable_qtgui_label_7_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_7_formatter = None
        else:
          self._variable_qtgui_label_7_formatter = lambda x: repr(x)

        self._variable_qtgui_label_7_tool_bar.addWidget(Qt.QLabel('experiment'+": "))
        self._variable_qtgui_label_7_label = Qt.QLabel(str(self._variable_qtgui_label_7_formatter(self.variable_qtgui_label_7)))
        self._variable_qtgui_label_7_tool_bar.addWidget(self._variable_qtgui_label_7_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_7_tool_bar)
        self._variable_qtgui_label_6_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_6_formatter = None
        else:
          self._variable_qtgui_label_6_formatter = lambda x: repr(x)

        self._variable_qtgui_label_6_tool_bar.addWidget(Qt.QLabel('event'+": "))
        self._variable_qtgui_label_6_label = Qt.QLabel(str(self._variable_qtgui_label_6_formatter(self.variable_qtgui_label_6)))
        self._variable_qtgui_label_6_tool_bar.addWidget(self._variable_qtgui_label_6_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_6_tool_bar)
        self._variable_qtgui_label_5_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_5_formatter = None
        else:
          self._variable_qtgui_label_5_formatter = lambda x: repr(x)

        self._variable_qtgui_label_5_tool_bar.addWidget(Qt.QLabel('freq_sqr'+": "))
        self._variable_qtgui_label_5_label = Qt.QLabel(str(self._variable_qtgui_label_5_formatter(self.variable_qtgui_label_5)))
        self._variable_qtgui_label_5_tool_bar.addWidget(self._variable_qtgui_label_5_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_5_tool_bar)
        self._variable_qtgui_label_4_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_4_formatter = None
        else:
          self._variable_qtgui_label_4_formatter = lambda x: repr(x)

        self._variable_qtgui_label_4_tool_bar.addWidget(Qt.QLabel('freq'+": "))
        self._variable_qtgui_label_4_label = Qt.QLabel(str(self._variable_qtgui_label_4_formatter(self.variable_qtgui_label_4)))
        self._variable_qtgui_label_4_tool_bar.addWidget(self._variable_qtgui_label_4_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_4_tool_bar)
        self._variable_qtgui_label_3_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_3_formatter = None
        else:
          self._variable_qtgui_label_3_formatter = lambda x: repr(x)

        self._variable_qtgui_label_3_tool_bar.addWidget(Qt.QLabel('dcycle'+": "))
        self._variable_qtgui_label_3_label = Qt.QLabel(str(self._variable_qtgui_label_3_formatter(self.variable_qtgui_label_3)))
        self._variable_qtgui_label_3_tool_bar.addWidget(self._variable_qtgui_label_3_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_3_tool_bar)
        self._variable_qtgui_label_21_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_21_formatter = None
        else:
          self._variable_qtgui_label_21_formatter = lambda x: repr(x)

        self._variable_qtgui_label_21_tool_bar.addWidget(Qt.QLabel('result_prefix'+": "))
        self._variable_qtgui_label_21_label = Qt.QLabel(str(self._variable_qtgui_label_21_formatter(self.variable_qtgui_label_21)))
        self._variable_qtgui_label_21_tool_bar.addWidget(self._variable_qtgui_label_21_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_21_tool_bar)
        self._variable_qtgui_label_2_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_2_formatter = None
        else:
          self._variable_qtgui_label_2_formatter = lambda x: repr(x)

        self._variable_qtgui_label_2_tool_bar.addWidget(Qt.QLabel('config_file_name'+": "))
        self._variable_qtgui_label_2_label = Qt.QLabel(str(self._variable_qtgui_label_2_formatter(self.variable_qtgui_label_2)))
        self._variable_qtgui_label_2_tool_bar.addWidget(self._variable_qtgui_label_2_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_2_tool_bar)
        self._variable_qtgui_label_1_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_1_formatter = None
        else:
          self._variable_qtgui_label_1_formatter = lambda x: repr(x)

        self._variable_qtgui_label_1_tool_bar.addWidget(Qt.QLabel('results_path'+": "))
        self._variable_qtgui_label_1_label = Qt.QLabel(str(self._variable_qtgui_label_1_formatter(self.variable_qtgui_label_1)))
        self._variable_qtgui_label_1_tool_bar.addWidget(self._variable_qtgui_label_1_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_1_tool_bar)
        self._variable_qtgui_label_0_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_0_formatter = None
        else:
          self._variable_qtgui_label_0_formatter = lambda x: repr(x)

        self._variable_qtgui_label_0_tool_bar.addWidget(Qt.QLabel('samp_rate'+": "))
        self._variable_qtgui_label_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0)))
        self._variable_qtgui_label_0_tool_bar.addWidget(self._variable_qtgui_label_0_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_0_tool_bar)
        self.sl_pwm_0 = sl_pwm(
            amp=1,
            delay_percent=0,
            duty_cycle=dcycle,
            freq=freq_sqr,
            samp=samp_rate,
        )
        self.red_pitaya_source_0_0_1 = red_pitaya.source(
                addr='192.168.5.100',
                port=1001,
                freq=center_freq,
                rate=samp_rate,
                corr=0
        )

        self.red_pitaya_sink_0 = red_pitaya.sink(
                addr='192.168.5.100',
                port=1001,
                freq=center_freq,
                rate=samp_rate,
                corr=0,
                ptt=True
        )

        self.qtgui_time_sink_x_0_2_0 = qtgui.time_sink_c(
        	2500, #size
        	samp_rate, #samp_rate
        	"TX", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0_2_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0_2_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0_2_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0_2_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_2_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0_2_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0_2_0.enable_grid(False)
        self.qtgui_time_sink_x_0_2_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_2_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0_2_0.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0_2_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                if(i % 2 == 0):
                    self.qtgui_time_sink_x_0_2_0.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0_2_0.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0_2_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_2_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_2_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_2_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_2_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_2_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_2_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_2_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_2_0_win)
        self.qtgui_time_sink_x_0_2 = qtgui.time_sink_c(
        	5000, #size
        	samp_rate, #samp_rate
        	"RX", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0_2.set_update_time(0.10)
        self.qtgui_time_sink_x_0_2.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0_2.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0_2.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_2.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0_2.enable_autoscale(False)
        self.qtgui_time_sink_x_0_2.enable_grid(False)
        self.qtgui_time_sink_x_0_2.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_2.enable_control_panel(False)
        self.qtgui_time_sink_x_0_2.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0_2.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                if(i % 2 == 0):
                    self.qtgui_time_sink_x_0_2.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0_2.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0_2.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_2.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_2.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_2.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_2.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_2.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_2_win = sip.wrapinstance(self.qtgui_time_sink_x_0_2.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_2_win)
        self.blocks_multiply_xx_0 = blocks.multiply_vff(1)
        self.blocks_float_to_complex_0_0 = blocks.float_to_complex(1)
        self.blocks_file_sink_0_1 = blocks.file_sink(gr.sizeof_gr_complex*1, result_prefix + "TX_OUT1.dat", False)
        self.blocks_file_sink_0_1.set_unbuffered(False)
        self.blocks_file_sink_0_0_1 = blocks.file_sink(gr.sizeof_gr_complex*1, result_prefix + "RX_IN1.dat", False)
        self.blocks_file_sink_0_0_1.set_unbuffered(False)
        self.analog_sig_source_x_0 = analog.sig_source_f(samp_rate, analog.GR_SIN_WAVE, freq, 1, 0)
        self.analog_const_source_x_0_0 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, 0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source_x_0_0, 0), (self.blocks_float_to_complex_0_0, 1))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.blocks_float_to_complex_0_0, 0), (self.blocks_file_sink_0_1, 0))
        self.connect((self.blocks_float_to_complex_0_0, 0), (self.qtgui_time_sink_x_0_2_0, 0))
        self.connect((self.blocks_float_to_complex_0_0, 0), (self.red_pitaya_sink_0, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.blocks_float_to_complex_0_0, 0))
        self.connect((self.red_pitaya_source_0_0_1, 0), (self.blocks_file_sink_0_0_1, 0))
        self.connect((self.red_pitaya_source_0_0_1, 0), (self.qtgui_time_sink_x_0_2, 0))
        self.connect((self.sl_pwm_0, 0), (self.blocks_multiply_xx_0, 1))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "burst")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_results_path(self):
        return self.results_path

    def set_results_path(self, results_path):
        self.results_path = results_path
        self._samp_rate_config = ConfigParser.ConfigParser()
        self._samp_rate_config.read(self.results_path + self.config_file_name)
        if not self._samp_rate_config.has_section('main'):
        	self._samp_rate_config.add_section('main')
        self._samp_rate_config.set('main', "samp_rate", str(None))
        self._samp_rate_config.write(open(self.results_path + self.config_file_name, 'w'))
        self.set_result_prefix(str(self.results_path) + "exp"+str(self.experiment) +  "_e"+str(self.event) +"_sr"+str(self.samp_rate)+"_dc"+str(self.dcycle)+"_fc"+str(self.center_freq)+"_fsqr"+str(self.freq_sqr) +  "_f" + str(self.freq) + "_")
        self._freq_sqr_config = ConfigParser.ConfigParser()
        self._freq_sqr_config.read(self.results_path + self.config_file_name)
        if not self._freq_sqr_config.has_section('main'):
        	self._freq_sqr_config.add_section('main')
        self._freq_sqr_config.set('main', "freq_sqr", str(None))
        self._freq_sqr_config.write(open(self.results_path + self.config_file_name, 'w'))
        self._freq_config = ConfigParser.ConfigParser()
        self._freq_config.read(self.results_path + self.config_file_name)
        if not self._freq_config.has_section('main'):
        	self._freq_config.add_section('main')
        self._freq_config.set('main', "freq", str(None))
        self._freq_config.write(open(self.results_path + self.config_file_name, 'w'))
        self._center_freq_config = ConfigParser.ConfigParser()
        self._center_freq_config.read(self.results_path + self.config_file_name)
        if not self._center_freq_config.has_section('main'):
        	self._center_freq_config.add_section('main')
        self._center_freq_config.set('main', "center_freq", str(None))
        self._center_freq_config.write(open(self.results_path + self.config_file_name, 'w'))
        self.set_variable_qtgui_label_1(self._variable_qtgui_label_1_formatter(self.results_path))
        self._experiment_config = ConfigParser.ConfigParser()
        self._experiment_config.read(self.results_path + self.config_file_name)
        if not self._experiment_config.has_section('main'):
        	self._experiment_config.add_section('main')
        self._experiment_config.set('main', "experiment", str(None))
        self._experiment_config.write(open(self.results_path + self.config_file_name, 'w'))
        self._event_config = ConfigParser.ConfigParser()
        self._event_config.read(self.results_path + self.config_file_name)
        if not self._event_config.has_section('main'):
        	self._event_config.add_section('main')
        self._event_config.set('main', "event", str(None))
        self._event_config.write(open(self.results_path + self.config_file_name, 'w'))

    def get_config_file_name(self):
        return self.config_file_name

    def set_config_file_name(self, config_file_name):
        self.config_file_name = config_file_name
        self._samp_rate_config = ConfigParser.ConfigParser()
        self._samp_rate_config.read(self.results_path + self.config_file_name)
        if not self._samp_rate_config.has_section('main'):
        	self._samp_rate_config.add_section('main')
        self._samp_rate_config.set('main', "samp_rate", str(None))
        self._samp_rate_config.write(open(self.results_path + self.config_file_name, 'w'))
        self._freq_sqr_config = ConfigParser.ConfigParser()
        self._freq_sqr_config.read(self.results_path + self.config_file_name)
        if not self._freq_sqr_config.has_section('main'):
        	self._freq_sqr_config.add_section('main')
        self._freq_sqr_config.set('main', "freq_sqr", str(None))
        self._freq_sqr_config.write(open(self.results_path + self.config_file_name, 'w'))
        self._freq_config = ConfigParser.ConfigParser()
        self._freq_config.read(self.results_path + self.config_file_name)
        if not self._freq_config.has_section('main'):
        	self._freq_config.add_section('main')
        self._freq_config.set('main', "freq", str(None))
        self._freq_config.write(open(self.results_path + self.config_file_name, 'w'))
        self._center_freq_config = ConfigParser.ConfigParser()
        self._center_freq_config.read(self.results_path + self.config_file_name)
        if not self._center_freq_config.has_section('main'):
        	self._center_freq_config.add_section('main')
        self._center_freq_config.set('main', "center_freq", str(None))
        self._center_freq_config.write(open(self.results_path + self.config_file_name, 'w'))
        self.set_variable_qtgui_label_2(self._variable_qtgui_label_2_formatter(self.config_file_name))
        self._experiment_config = ConfigParser.ConfigParser()
        self._experiment_config.read(self.results_path + self.config_file_name)
        if not self._experiment_config.has_section('main'):
        	self._experiment_config.add_section('main')
        self._experiment_config.set('main', "experiment", str(None))
        self._experiment_config.write(open(self.results_path + self.config_file_name, 'w'))
        self._event_config = ConfigParser.ConfigParser()
        self._event_config.read(self.results_path + self.config_file_name)
        if not self._event_config.has_section('main'):
        	self._event_config.add_section('main')
        self._event_config.set('main', "event", str(None))
        self._event_config.write(open(self.results_path + self.config_file_name, 'w'))

    def get_freq_sqr(self):
        return self.freq_sqr

    def set_freq_sqr(self, freq_sqr):
        self.freq_sqr = freq_sqr
        self.set_result_prefix(str(self.results_path) + "exp"+str(self.experiment) +  "_e"+str(self.event) +"_sr"+str(self.samp_rate)+"_dc"+str(self.dcycle)+"_fc"+str(self.center_freq)+"_fsqr"+str(self.freq_sqr) +  "_f" + str(self.freq) + "_")
        self.set_dcycle(5.*self.freq_sqr/self.freq)
        self.set_variable_qtgui_label_5(self._variable_qtgui_label_5_formatter(self.freq_sqr))
        self.sl_pwm_0.set_freq(self.freq_sqr)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.set_result_prefix(str(self.results_path) + "exp"+str(self.experiment) +  "_e"+str(self.event) +"_sr"+str(self.samp_rate)+"_dc"+str(self.dcycle)+"_fc"+str(self.center_freq)+"_fsqr"+str(self.freq_sqr) +  "_f" + str(self.freq) + "_")
        self.set_dcycle(5.*self.freq_sqr/self.freq)
        self.set_variable_qtgui_label_4(self._variable_qtgui_label_4_formatter(self.freq))
        self.analog_sig_source_x_0.set_frequency(self.freq)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_result_prefix(str(self.results_path) + "exp"+str(self.experiment) +  "_e"+str(self.event) +"_sr"+str(self.samp_rate)+"_dc"+str(self.dcycle)+"_fc"+str(self.center_freq)+"_fsqr"+str(self.freq_sqr) +  "_f" + str(self.freq) + "_")
        self.set_variable_qtgui_label_0(self._variable_qtgui_label_0_formatter(self.samp_rate))
        self.sl_pwm_0.set_samp(self.samp_rate)
        self.red_pitaya_source_0_0_1.set_rate(self.samp_rate)
        self.red_pitaya_sink_0.set_rate(self.samp_rate)
        self.qtgui_time_sink_x_0_2_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_0_2.set_samp_rate(self.samp_rate)
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)

    def get_experiment(self):
        return self.experiment

    def set_experiment(self, experiment):
        self.experiment = experiment
        self.set_result_prefix(str(self.results_path) + "exp"+str(self.experiment) +  "_e"+str(self.event) +"_sr"+str(self.samp_rate)+"_dc"+str(self.dcycle)+"_fc"+str(self.center_freq)+"_fsqr"+str(self.freq_sqr) +  "_f" + str(self.freq) + "_")
        self.set_variable_qtgui_label_7(self._variable_qtgui_label_7_formatter(self.experiment))

    def get_event(self):
        return self.event

    def set_event(self, event):
        self.event = event
        self.set_result_prefix(str(self.results_path) + "exp"+str(self.experiment) +  "_e"+str(self.event) +"_sr"+str(self.samp_rate)+"_dc"+str(self.dcycle)+"_fc"+str(self.center_freq)+"_fsqr"+str(self.freq_sqr) +  "_f" + str(self.freq) + "_")
        self.set_variable_qtgui_label_6(self._variable_qtgui_label_6_formatter(self.event))

    def get_dcycle(self):
        return self.dcycle

    def set_dcycle(self, dcycle):
        self.dcycle = dcycle
        self.set_result_prefix(str(self.results_path) + "exp"+str(self.experiment) +  "_e"+str(self.event) +"_sr"+str(self.samp_rate)+"_dc"+str(self.dcycle)+"_fc"+str(self.center_freq)+"_fsqr"+str(self.freq_sqr) +  "_f" + str(self.freq) + "_")
        self.set_variable_qtgui_label_3(self._variable_qtgui_label_3_formatter(self.dcycle))
        self.sl_pwm_0.set_duty_cycle(self.dcycle)

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.set_result_prefix(str(self.results_path) + "exp"+str(self.experiment) +  "_e"+str(self.event) +"_sr"+str(self.samp_rate)+"_dc"+str(self.dcycle)+"_fc"+str(self.center_freq)+"_fsqr"+str(self.freq_sqr) +  "_f" + str(self.freq) + "_")
        self.set_variable_qtgui_label_8(self._variable_qtgui_label_8_formatter(self.center_freq))
        self.red_pitaya_source_0_0_1.set_freq(self.center_freq, 0)
        self.red_pitaya_sink_0.set_freq(self.center_freq, 0)

    def get_result_prefix(self):
        return self.result_prefix

    def set_result_prefix(self, result_prefix):
        self.result_prefix = result_prefix
        self.set_variable_qtgui_label_21(self._variable_qtgui_label_21_formatter(self.result_prefix))
        self.blocks_file_sink_0_1.open(self.result_prefix + "TX_OUT1.dat")
        self.blocks_file_sink_0_0_1.open(self.result_prefix + "RX_IN1.dat")

    def get_variable_qtgui_label_8(self):
        return self.variable_qtgui_label_8

    def set_variable_qtgui_label_8(self, variable_qtgui_label_8):
        self.variable_qtgui_label_8 = variable_qtgui_label_8
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_8_label, "setText", Qt.Q_ARG("QString", self.variable_qtgui_label_8))

    def get_variable_qtgui_label_7(self):
        return self.variable_qtgui_label_7

    def set_variable_qtgui_label_7(self, variable_qtgui_label_7):
        self.variable_qtgui_label_7 = variable_qtgui_label_7
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_7_label, "setText", Qt.Q_ARG("QString", self.variable_qtgui_label_7))

    def get_variable_qtgui_label_6(self):
        return self.variable_qtgui_label_6

    def set_variable_qtgui_label_6(self, variable_qtgui_label_6):
        self.variable_qtgui_label_6 = variable_qtgui_label_6
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_6_label, "setText", Qt.Q_ARG("QString", self.variable_qtgui_label_6))

    def get_variable_qtgui_label_5(self):
        return self.variable_qtgui_label_5

    def set_variable_qtgui_label_5(self, variable_qtgui_label_5):
        self.variable_qtgui_label_5 = variable_qtgui_label_5
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_5_label, "setText", Qt.Q_ARG("QString", self.variable_qtgui_label_5))

    def get_variable_qtgui_label_4(self):
        return self.variable_qtgui_label_4

    def set_variable_qtgui_label_4(self, variable_qtgui_label_4):
        self.variable_qtgui_label_4 = variable_qtgui_label_4
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_4_label, "setText", Qt.Q_ARG("QString", self.variable_qtgui_label_4))

    def get_variable_qtgui_label_3(self):
        return self.variable_qtgui_label_3

    def set_variable_qtgui_label_3(self, variable_qtgui_label_3):
        self.variable_qtgui_label_3 = variable_qtgui_label_3
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_3_label, "setText", Qt.Q_ARG("QString", self.variable_qtgui_label_3))

    def get_variable_qtgui_label_21(self):
        return self.variable_qtgui_label_21

    def set_variable_qtgui_label_21(self, variable_qtgui_label_21):
        self.variable_qtgui_label_21 = variable_qtgui_label_21
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_21_label, "setText", Qt.Q_ARG("QString", self.variable_qtgui_label_21))

    def get_variable_qtgui_label_2(self):
        return self.variable_qtgui_label_2

    def set_variable_qtgui_label_2(self, variable_qtgui_label_2):
        self.variable_qtgui_label_2 = variable_qtgui_label_2
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_2_label, "setText", Qt.Q_ARG("QString", self.variable_qtgui_label_2))

    def get_variable_qtgui_label_1(self):
        return self.variable_qtgui_label_1

    def set_variable_qtgui_label_1(self, variable_qtgui_label_1):
        self.variable_qtgui_label_1 = variable_qtgui_label_1
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_1_label, "setText", Qt.Q_ARG("QString", self.variable_qtgui_label_1))

    def get_variable_qtgui_label_0(self):
        return self.variable_qtgui_label_0

    def set_variable_qtgui_label_0(self, variable_qtgui_label_0):
        self.variable_qtgui_label_0 = variable_qtgui_label_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_label, "setText", Qt.Q_ARG("QString", self.variable_qtgui_label_0))


def main(top_block_cls=burst, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
